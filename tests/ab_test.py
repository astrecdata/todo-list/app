import subprocess

app_url = "http://127.0.0.1:3000/"

ab_command = f"ab -n 1000 -c 100 {app_url}"

process = subprocess.Popen(ab_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

stdout, stderr = process.communicate()

if process.returncode != 0:
    print("Error output:")
    print(stderr.decode())
    exit(1)
else:
    print("Results (Apache Benchmark):")
    print(stdout.decode())
