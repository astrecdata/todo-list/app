from selenium import webdriver
from selenium.webdriver import FirefoxOptions
from selenium.webdriver.common.keys import Keys
import time

opts = FirefoxOptions()
opts.set_headless()
browser = webdriver.Firefox(options=opts)
try:
    browser.get("http://127.0.0.1:3000/")
    input_field = browser.find_element_by_css_selector("input[placeholder='Add a new todo...']")
    input_field.send_keys("Test text")
    input_field.send_keys(Keys.ENTER)
    time.sleep(1)
    div_element = browser.find_element_by_xpath("//div[text()='Test text']")
except Exception as e:
    print("Error occured during test: ", e)
finally:
    browser.quit()
