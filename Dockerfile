FROM node:21.7
WORKDIR /todo-list
COPY package*.json ./
RUN npm install -g @quasar/cli
RUN npm install
COPY . .
RUN quasar build -m ssr
EXPOSE 3000
CMD ["quasar", "serve", "-m", "ssr", "dist/ssr/"]
